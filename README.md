# Multi-source local high-resolution scenario with emphasis on the 2022 Tonga case

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/NGI.png?inline=false" width="60">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/TUM.png?inline=false" width="80">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="35">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/UMA.png?inline=false" width="100">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/LMU.png?inline=false" width="80">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/RBI.png?inline=false" width="100">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="100">

**Codes:** [Tsunami-HySEA](https://gitlab.com/cheese5311126/codes/hysea/tsunami-hysea), Meteo-HySEA, [ExaHype](https://gitlab.com/cheese5311126/codes/exahype/exahype), [SeisSol](https://gitlab.com/cheese5311126/codes/seissol/seissol)

**Target EuroHPC Architectures:** Leonardo, MN5

**Type:** EWS, urgent computing

## Description

This SC will simulate source mechanics, tsunamigenesis, propagation, and
inundation from different coupled tsunamigenic physical processes including
complex earthquake rupture physics, landslides, and volcanic explosions. The
objective of SC4.1 is to realise a set of simulation cases where different types of
physics interact, and that couple the input and output of different ChEESE-2P
flagship codes to realise this. To this end, the main simulation case will be the
2022 Hunga Tonga Hunga Ha'apai (HTHH) eruption that generated tsunamis
both locally (through flank collapse) and globally (through atmospheric-oceanic
coupling from the generated pressure wave). We will perform a systematic suite
of landslide simulations using the ExaHyPE engine, studying sensitivity to
different collapse scenarios, and simulate the local and regional tsunami
propagation using the Multilayer-HySEA code, forced by the landslide simulation
output from ExaHyPE. We will simulate the oceanic scale tsunami, including
inundation simulations, with forcing both from the output of the local/regional
tsunami simulation and from the atmospheric pressure, using the Meteo-HySEA
code. We will in addition simulate a set of other high-impact application cases
where multiphysics or model coupling is necessary. These include landslide
tsunamis from flank collapses from Stromboli, and complex time dependent
earthquake rupture and tsunami generation from 3D dynamic rupture models
(e.g. scenarios for the Hellenic Arc and the 2010 Maule, Chile, earthquakes and
tsunamis) using the SeisSol code, and also perform systematic tests of fully
coupled 3D acoustic-elastic simulations.

<img src="SC4.1.png">

**Figure 3.6.1.** Graphical abstract of SC4.1.

## Expected results

The Tonga case will involve high-resolution coupling of flagship codes for acoustic pressure waves and
seismic rupture (SeisSol), landslides (HySEA or ExaHype), and coupled far field tsunami propagation forced by atmospheric
pressure waves (HySEA). HySEA will be updated to take into account atmospheric forcing in PD4 (T5.1). A fine-resolution
local simulation resolving the acoustic pressure wave, landslide dynamics, as well as a heuristic modeling of the caldera
collapse and local tsunami generation will need to be coupled to a global-scale propagation model for the atmospheric
pressure wave and tsunami. Combined, this will require very large computational resources to resolve a single simulation,
which implies a push towards testing the simulation using pre-exascale computational resources. On the other hand, through
the capacity simulations, the SC will also gain new insight into complex multiphysics phenomena (e.g. high resolution
atmospheric pressure wave, earthquake dynamics, landslides, all coupled to tsunami generation) that cannot be described
only by single source models and will establish one potential service for UC applications in the EuroHPC context.